
class DrinkManager {
    int m_drinkSize = 10; //ml
    int m_drinkSpeed = 100;

    public:
        int getDrinkDelay();

        int getDrinkSize();
        int getDrinkSpeed();

        int setDrinkSize(int size);
        int setDrinkSpeed(int speed);
};