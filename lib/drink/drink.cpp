#include <drink.h>

// Get drink delay for target drink size and drink speed in milliseconds
int DrinkManager::getDrinkDelay() {
    return 10000*m_drinkSize/m_drinkSpeed;
}

int DrinkManager::getDrinkSize() {
    return m_drinkSize;
}

int DrinkManager::getDrinkSpeed() {
    return m_drinkSpeed;
}

int DrinkManager::setDrinkSize(int size) {
    m_drinkSize = size;
    return m_drinkSize;
}

int DrinkManager::setDrinkSpeed(int speed) {
    m_drinkSpeed = speed;
    return m_drinkSpeed;
}