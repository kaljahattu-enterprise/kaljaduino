#include <bluetooth.h>

HatBluetoothManager::HatBluetoothManager(char* UUID) : m_UUID(UUID) {}

int HatBluetoothManager::initalizeBLE() { return BLE.begin(); };
void HatBluetoothManager::pollBLE() { BLE.poll(); };

bool HatBluetoothManager::initializeService() { 
    BLE.setLocalName(m_name);
    BLE.setAdvertisedService(hatService);

    hatService.addCharacteristic(motorStateCharacteristic);
    hatService.addCharacteristic(motorSpeedCharacteristic);
    hatService.addCharacteristic(motorDirectionCharacteristic);

    hatService.addCharacteristic(drinkSizeCharacteristic);
    hatService.addCharacteristic(drinkSpeedCharacteristic);
    hatService.addCharacteristic(drinkStateCharacteristic);

    BLE.addService(hatService);

    motorStateCharacteristic.writeValue(false);
    motorSpeedCharacteristic.writeValue(100);
    motorDirectionCharacteristic.writeValue(false);

    drinkSizeCharacteristic.writeValue(0);
    drinkSpeedCharacteristic.writeValue(100);
    drinkStateCharacteristic.writeValue(0);

    return BLE.advertise();
};

// All getters

const char*& HatBluetoothManager::getName() { return m_name; };
const char*& HatBluetoothManager::getUUID() { return m_UUID; };
int HatBluetoothManager::getMotorState()       { return motorStateCharacteristic.value(); };
int HatBluetoothManager::getMotorSpeed()       { return motorSpeedCharacteristic.value(); };
int HatBluetoothManager::getMotorDirection()   { return motorDirectionCharacteristic.value(); };

int HatBluetoothManager::getDrinkSize()        { return drinkSizeCharacteristic.value(); };
int HatBluetoothManager::getDrinkSpeed()       { return drinkSpeedCharacteristic.value(); };
int HatBluetoothManager::getDrinkState()       { return drinkStateCharacteristic.value(); };

// All setters

void HatBluetoothManager::setMotorState(int state) {
    motorStateCharacteristic.writeValue(state);
};

void HatBluetoothManager::setMotorSpeed(int speed) {
    motorSpeedCharacteristic.writeValue(speed);
};

void HatBluetoothManager::setMotorDirection(int direction) {
    motorDirectionCharacteristic.writeValue(direction);
};

void HatBluetoothManager::setDrinkSize(int size) {
    drinkSizeCharacteristic.writeValue(size);
};

void HatBluetoothManager::setDrinkSpeed(int speed) {
    drinkSpeedCharacteristic.writeValue(speed);
};

void HatBluetoothManager::setDrinkState(int state) {
    drinkStateCharacteristic.writeValue(state);
};

void HatBluetoothManager::setMotorStateCallback(BLECharacteristicEventHandler handler) {
    motorStateCharacteristic.setEventHandler(BLEWritten, handler);
};
void HatBluetoothManager::setMotorSpeedCallback(BLECharacteristicEventHandler handler) {
    motorSpeedCharacteristic.setEventHandler(BLEWritten, handler);
};
void HatBluetoothManager::setMotorDirectionCallback(BLECharacteristicEventHandler handler) {
    motorDirectionCharacteristic.setEventHandler(BLEWritten, handler);
};
void HatBluetoothManager::setDrinkSizeCallback(BLECharacteristicEventHandler handler) {
    drinkSizeCharacteristic.setEventHandler(BLEWritten, handler);
};
void HatBluetoothManager::setDrinkSpeedCallback(BLECharacteristicEventHandler handler) {
    drinkSpeedCharacteristic.setEventHandler(BLEWritten, handler);
};
void HatBluetoothManager::setDrinkStateCallback(BLECharacteristicEventHandler handler) {
    drinkStateCharacteristic.setEventHandler(BLEWritten, handler);
};